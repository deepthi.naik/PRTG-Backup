PRTG Custom Sensor for backing up PRTG.
===========================================

Please document and rename this file to "README.md"

This project contains all the files necessary to integrate the Custom Script into PRTG.

The script will be installed as a Sensor with a parameter of the target directory.

Within the target directory, there will be a hierarchy as outlined below.

BackupLog   : Text output from backup process, one file per day... (RoboCopy output)
Data		: Files from PRTG DataDirectory
EXE			: Files from the PRTG Executable directory

./BackupLog:
yyyyMMdd_PRTGBackup.Log   : Backup output log from given day

./Data:
Configuration Auto-Backups
Monitoring Database
Ticket Database
Trap Database
...

./EXE:
Custom Sensors
Download
PRTG Installer Archive
devicetemplates
lookups
snmplibs
webroot
...

./EXE/webroot/includes:
htmlfooter_custom_v2.htm
htmlheader_custom_v2.htm

Download Instructions
=========================
 A zip file containing all the files in the project can be downloaded from the 
repository(https://gitlab.com/PRTG-Projects/Sensor-Scripts/PRTG-Backup/-/jobs/artifacts/master/download?job=PRTGDistZip) 
download link

Installation Instructions
=========================
Please refer to INSTALL.md
