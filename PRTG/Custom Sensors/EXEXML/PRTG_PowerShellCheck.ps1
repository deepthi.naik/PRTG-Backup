  $SessionSize=  32
  if ([System.IntPtr]::Size -eq 8)
    {
      $SessionSize=  64
    }
 $PS_Major = ""+$Host.Version.Major
 $PS_Minor = ""+$Host.Version.Minor
 $PS_Full = " User["+ $env:USERDOMAIN+"\"+$env:USERNAME+"] Full Version"+$Host.Version
echo @"
<prtg>
  <result>
	<channel>PS Execution Mode</channel>
	<value>$SessionSize</value>
  </result>
  <result>
	<channel>PS Version Major</channel>
	<value>$PS_Major</value>
  </result>  
  <result>
	<channel>PS Version Minor</channel>
	<value>$PS_Minor</value>
  </result>
  <text>$PS_Full</text>
</prtg>
"@